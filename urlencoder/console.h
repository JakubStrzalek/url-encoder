#ifndef CONSOLE_H
#define CONSOLE_H

#include <exception>
#include <string>

class Console
{
private:
    const int ACCEPTABLE_ARGUMENTS_NUMBER = 3;

    int size;
    char **arguments;
    std::string inputLocation;
    std::string outputDirectory;
public:

    const int checkArgumentsCorrectness() throw(std::exception);
    Console(int size, char *arguments[]) : size(size), arguments(arguments)
    {
        this->inputLocation = std::string(arguments[1]);
        this->outputDirectory = std::string(arguments[2]);
    }

    std::string getInputLocation();
    std::string getOutputDirectory();
};

#endif // CONSOLE_H
