#ifndef OUTPUTDIRECTORYMANAGER_H
#define OUTPUTDIRECTORYMANAGER_H

#include <string>

class OutputDirectoryManager
{
private:
    int previousValue = 0;

    std::string directory;

    const int MAX_NUMBER_OF_DIGIT = 6;
    const int MAX_EXPRESSION_NUMBER = 999999;
    std::string format(int number);
    int getNextValue() throw(std::exception);
public:
    OutputDirectoryManager(std::string directory) : directory(directory) {}
    void saveFile(std::string value);
};

#endif // OUTPUTDIRECTORYMANAGER_H
