#include "urlencoder.h"
#include <iostream>

void URLencoder::prepareInputAndOutput()
{
    this->inputFile = new InputFileManager(this->console->getInputLocation());
    this->outputDirectory = new OutputDirectoryManager(this->console->getOutputDirectory());
}

void URLencoder::prepareParser()
{
    this->parser = new Parser(this->inputFile,this->outputDirectory);
}

URLencoder::URLencoder(int size, char **arguments) : console(new Console(size,arguments))
{
    this->console->checkArgumentsCorrectness();
    this->prepareInputAndOutput();
    this->prepareParser();

}

URLencoder::~URLencoder()
{
    delete this->console;
    delete this->inputFile;
    delete this->outputDirectory;
}

const int URLencoder::run() const
{
    try
    {
        //testowane i działa

        this->parser->run();
        //
        //Wywalac exception jak nie istnieja odpowiednie directories
    }
    catch(std::exception e)
    {
        std::cerr<<e.what()<<std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
