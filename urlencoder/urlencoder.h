#ifndef URLENCODER_H
#define URLENCODER_H

#include "console.h"
#include "filemanager/inputfilemanager.h"
#include "outputdirectorymanager.h"
#include "parser/parser.h"

class URLencoder
{
private:
    Console *console;
    InputFileManager *inputFile;
    OutputDirectoryManager *outputDirectory;
    Parser *parser;

    void prepareInputAndOutput();
    void prepareParser();
public:
    URLencoder(int size,char **arguments);
    ~URLencoder();
    const int run() const;
};

#endif // URLENCODER_H
