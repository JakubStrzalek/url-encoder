#include "console.h"

#include "exceptions/invalidargumentsnumberexception.h"


const int Console::checkArgumentsCorrectness() throw(std::exception)
{
    if(this->size != ACCEPTABLE_ARGUMENTS_NUMBER)
    {
        throw new InvalidArgumentsNumberException();
    }

    return 0;
}

std::string Console::getInputLocation()
{
    return this->inputLocation;
}

std::string Console::getOutputDirectory()
{
    return this->outputDirectory;
}
