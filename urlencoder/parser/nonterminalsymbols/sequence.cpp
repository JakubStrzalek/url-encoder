#include "sequence.h"

#include "statement.h"
#include <cctype>

//Sequence = Statement, { { Not_ASCII_Sign }1, Statement };
void Sequence::parse() throw(std::exception)
{
    statement->parse();

    //tutaj mozemy zakonczyc i bedzie dobrze
    ch = connector->get();
    while( isNotASCII(ch) )
    {
        //tutaj mam juz pewnosc ze jest minimalne jeden Not_ASCII_Sign
        ch = connector->get();

        while( isNotASCII(ch) )
        {		//wczytuj znaki aż nie natrafisz na
            ch = connector->get();

        }

        if(isASCII(ch)) {
            connector->moveIndicatorBack(1);
            statement->parse();
            ch = connector->get();
        }
    }
}
