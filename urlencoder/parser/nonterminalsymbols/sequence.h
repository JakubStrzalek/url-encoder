#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "nonterminalsymbol.h"
#include "parser/parserconnector.h"
#include "statement.h"

class Sequence : NonTerminalSymbol
{
private:
    Statement *statement;
public:
    Sequence(ParserConnector *connector) : NonTerminalSymbol(connector) {
        statement = new Statement(connector);
    }

    virtual void parse() throw (std::exception) override;
};

#endif // SEQUENCE_H
