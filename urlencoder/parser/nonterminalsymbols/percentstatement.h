#ifndef PERCENTSTATEMENT_H
#define PERCENTSTATEMENT_H

#include "nonterminalsymbol.h"

class PercentStatement : NonTerminalSymbol
{
private:
    bool isHexFirst(char ch) const;
    bool isHexSecond(char firstHex, char secondHex) const;
public:
    PercentStatement(ParserConnector *connector) : NonTerminalSymbol(connector) {}

    virtual void parse() throw(std::exception) override;
};

#endif // PERCENTSTATEMENT_H
