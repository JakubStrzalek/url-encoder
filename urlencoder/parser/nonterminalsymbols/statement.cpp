#include "statement.h"

#include <cctype>

Statement::~Statement()
{
    delete percentStatement;
}
//{ Sign | Percent_Statement }1
void Statement::parse() throw (std::exception)
{
    ch = connector->get();

    while(isASCII(ch)) {
        if(ch == '%') {
            //Percent_Statement
            connector->moveIndicatorBack(1);
            percentStatement->parse();
        } else {
            //Kolejny Sign
            this->connector->addToSolution(ch);
        }
        ch = connector->get();;
    }

    //Pobralismy cos nie bedace ani Sign ani Percent_Statement
    //dlatego cofnijmy wskaznik o 1
    connector->moveIndicatorBack(1);
    connector->saveSolution();
}
