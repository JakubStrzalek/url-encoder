#include "file.h"
#include <iostream>
#include "exceptions/parseexception.h"

File::~File()
{
    delete this->sequence;
}

//File = { Not_ASCII_Sign }, Sequence, { Not_ASCII_Sign } | { Not_ASCII_Sign } ;
void File::parse() throw(std::exception)
{
    ch = connector->get();
    while( isNotASCII(ch) ) {		//Wczytuj znaki do momentu
                            //aż nie natrafisz na znak ASCII
        ch = connector->get();
    }
    //2 mozliwosci: eof albo isprint

    if(isASCII(ch)) {
        //tutaj juz jest spelniona pierwsza produkcja
        //{ Not_ASCII_Sign }, Sequence, { Not_ASCII_Sign }
        //trzeci czlon omijamy bo nie zmienia plikow wynikowych
        connector->moveIndicatorBack(1);

        sequence->parse();

//        while( !isprint(ch) ) {		//Wczytuj znaki do momentu
//                                //aż nie natrafisz na znak ASCII
//            ch = connector->getUnsafe();
//        }
        //2 mozliwosci: eof al
    } else {
        //eof
        //druga produkcja
        //{ Not_ASCII_Sign }
    }
}
