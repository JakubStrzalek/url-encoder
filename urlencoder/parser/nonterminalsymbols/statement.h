#ifndef STATEMENT_H
#define STATEMENT_H

#include "nonterminalsymbol.h"
#include "percentstatement.h"


class Statement : NonTerminalSymbol
{
private:
    PercentStatement *percentStatement;
public:
    Statement(ParserConnector *connector) : NonTerminalSymbol(connector) {
        percentStatement = new PercentStatement(connector);
    }

    ~Statement();
    virtual void parse() throw (std::exception);
};

#endif // STATEMENT_H
