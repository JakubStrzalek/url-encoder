#ifndef FILE_H
#define FILE_H

#include "nonterminalsymbol.h"
#include "parser/parserconnector.h"
#include "sequence.h"

class File : public NonTerminalSymbol
{
private:
    Sequence *sequence;
public:
    File(ParserConnector *connector) : NonTerminalSymbol(connector) {
        sequence = new Sequence(connector);
    }

    ~File();
    virtual void parse() throw (std::exception) override;
};

#endif // FILE_H
