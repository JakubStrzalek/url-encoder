#include "percentstatement.h"
#include <string>


bool PercentStatement::isHexFirst(char ch) const
{
    //Zakladam ze pierwszy Hex jest od 2 do 7
    if(ch >= '2' && ch <= '7') {
        return true;
    }

    return false;
}

bool PercentStatement::isHexSecond(char firstHex,char secondHex) const
{
    if(secondHex >= '0' && secondHex <= '9') {
        return true;
    }
    if(secondHex >= 'A' && secondHex <= 'E') {
        return true;
    }
    if(secondHex == 'F' && firstHex !='7') {
        return true;
        //chce ominac znak DEL ktory w hexie nie ma postaci
    }

    return false;
}

//Percent_Statement = { "%" }1, ( Hex, Hex |  Not_Hex |  Hex, Not_Hex  ) ;
void PercentStatement::parse() throw(std::exception)
{
    std::string totranslate("%");
    //Z przejscia wyzej mamy gwarancje minimum jednego %, zatem napewno {%}1
    ch = connector->get();
    while(ch == '%') {
        ch = connector->get();
        if(ch == '%') connector->addToSolution('%');
        //zapisz procent jesli ch == %, jak nie to dodaj do stringa totranslate
    }

    char firstch = ch;
    char secondch;
    if(isHexFirst(firstch)) {
        secondch = connector->get();
        if(isHexSecond(firstch,secondch)) {
            //translacja
            totranslate += firstch;
            totranslate += secondch;
            char translated = this->connector->translateURL(totranslate);
            //i dodawanie
            connector->addToSolution(translated);

        } else {
            //Not_Hex albo Not_ASCII_Sign
            connector->addToSolution('%');
            connector->moveIndicatorBack(2);
        }

    } else {
        connector->addToSolution('%');
        connector->moveIndicatorBack(1);
        //Confnij
    }
}
