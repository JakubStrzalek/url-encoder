#ifndef NONTERMINALSYMBOL_H
#define NONTERMINALSYMBOL_H

#include "parser/parserconnector.h"
#include <cctype>

#include <iostream>

class NonTerminalSymbol
{
protected:
    ParserConnector *connector;
    char ch;

public:
    NonTerminalSymbol(ParserConnector *connector) : connector(connector) {}

    virtual void parse() throw (std::exception) = 0;

    bool isASCII(char ch) {
        return (isprint(ch));
    }

    bool isNotASCII(char ch) {
//        if(!connector->isEof()) {
//            std::cout<<"Plik nie osiagnal eof, obecny char to: "<<ch;
//        }
        return (!isprint(ch) && !connector->isEof());
    }

};

#endif // NONTERMINALSYMBOL_H
