#ifndef PARSER_H
#define PARSER_H

#include "parserconnector.h"
#include "filemanager/inputfilemanager.h"
#include "outputdirectorymanager.h"

class Parser : ParserConnector
{
private:
    InputFileManager *file;
    OutputDirectoryManager *output;

    bool flagSave;

    std::string solution;
public:
    Parser(InputFileManager* file, OutputDirectoryManager* output) : file(file), output(output), flagSave(false) {}

    virtual char get() override;
    virtual bool isEof() override;
    virtual void moveIndicatorBack(int number) override;

    virtual char translateURL(std::string totranslate) override;
    virtual void addToSolution(char ch) override;
    virtual void saveSolution() override;

    int run();
//    virtual void moveIndicator() override;
};

#endif // PARSER_H
