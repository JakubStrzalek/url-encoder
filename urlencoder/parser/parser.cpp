#include "parser.h"

#include "nonterminalsymbols/file.h"
#include "exceptions/parseexception.h"


char Parser::get()
{
    return this->file->read();
}

bool Parser::isEof()
{
    return this->file->iseof();
}

void Parser::moveIndicatorBack(int number)
{
    //Jest to niezbedne; gdyby nie ten blok to wystapilby blad z eof przy cofaniu
    if(this->isEof())
        return;

    return this->file->moveIndicatorBack(number);
}

char Parser::translateURL(std::string totranslate)
{
    std::string seq = "0x" + totranslate.substr(1,2);
    char character = std::stoi(seq,nullptr,0);

    this->flagSave = true;

    return character;

}

void Parser::addToSolution(char ch)
{
    this->solution += ch;
}

void Parser::saveSolution()
{
    std::string tosave = this->solution;
    this->solution.clear();

    if(!flagSave) {
        return;
    }

    this->output->saveFile(tosave);
    this->flagSave = false;
}

int Parser::run()
{
    File file(this);
    file.parse();
}

