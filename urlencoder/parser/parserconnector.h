#ifndef PARSERCONNECTOR_H
#define PARSERCONNECTOR_H

#include <string>

class ParserConnector
{

public:
    ParserConnector();

    //Input
    virtual char get() = 0;
    virtual bool isEof() = 0;
    virtual void moveIndicatorBack(int number) = 0;

    //Output and logic
    virtual char translateURL(std::string) = 0;
    virtual void addToSolution(char ch) = 0;
    virtual void saveSolution() = 0;

};

#endif // PARSERCONNECTOR_H
