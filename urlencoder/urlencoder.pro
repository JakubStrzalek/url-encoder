TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    urlencoder.cpp \
    console.cpp \
    exceptions/invalidargumentsnumberexception.cpp \
    outputdirectorymanager.cpp \
    exceptions/toomanypatternsexception.cpp \
    filemanager/filemanager.cpp \
    filemanager/inputfilemanager.cpp \
    filemanager/outputfilemanager.cpp \
    exceptions/inputfiledoesntexistexception.cpp \
    parser/parser.cpp \
    parser/nonterminalsymbols/file.cpp \
    parser/nonterminalsymbols/sequence.cpp \
    parser/nonterminalsymbols/statement.cpp \
    parser/parserconnector.cpp \
    parser/nonterminalsymbols/nonterminalsymbol.cpp \
    exceptions/parseexception.cpp \
    parser/nonterminalsymbols/percentstatement.cpp

HEADERS += \
    urlencoder.h \
    console.h \
    exceptions/invalidargumentsnumberexception.h \
    outputdirectorymanager.h \
    exceptions/toomanypatternsexception.h \
    filemanager/filemanager.h \
    filemanager/inputfilemanager.h \
    filemanager/outputfilemanager.h \
    exceptions/inputfiledoesntexistexception.h \
    parser/parser.h \
    parser/nonterminalsymbols/file.h \
    parser/nonterminalsymbols/sequence.h \
    parser/nonterminalsymbols/statement.h \
    parser/parserconnector.h \
    parser/nonterminalsymbols/nonterminalsymbol.h \
    exceptions/parseexception.h \
    parser/nonterminalsymbols/percentstatement.h

CONFIG += c++11
