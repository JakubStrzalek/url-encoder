#ifndef OUTPUTFILEMANAGER_H
#define OUTPUTFILEMANAGER_H

#include "filemanager.h"

class OutputFileManager : FileManager
{
public:
    OutputFileManager(std::string location);

    void write(std::string value);
};

#endif // OUTPUTFILEMANAGER_H
