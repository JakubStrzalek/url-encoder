#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#include <fstream>
#include <iostream>

class FileManager
{
protected:
    std::fstream *file;
public:
    FileManager() : file(new std::fstream) {}
    virtual ~FileManager();
};

#endif // FILEMANAGER_H
