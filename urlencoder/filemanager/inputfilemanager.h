#ifndef INPUTFILEMANAGER_H
#define INPUTFILEMANAGER_H

#include "filemanager.h"

class InputFileManager : FileManager
{
public:
    InputFileManager(std::string location);

    const char read();
    void moveIndicatorBack(int number);
    bool iseof() const;
};

#endif // INPUTFILEMANAGER_H
