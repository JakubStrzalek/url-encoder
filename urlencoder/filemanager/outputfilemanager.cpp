#include "outputfilemanager.h"
#include <fstream>

void OutputFileManager::write(std::string value)
{
    this->file->write(value.c_str(),value.length());

}

OutputFileManager::OutputFileManager(std::string location) : FileManager()
{
    this->file->open(location.c_str(), std::fstream::out | std::fstream::trunc);

}
