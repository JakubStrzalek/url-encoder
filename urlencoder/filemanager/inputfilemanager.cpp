#include "inputfilemanager.h"
#include <fstream>
#include "exceptions/inputfiledoesntexistexception.h"

InputFileManager::InputFileManager(std::string location) : FileManager()
{
    this->file->open(location.c_str(), std::fstream::in | std::ifstream::binary);

    if(!file->is_open()) {
        throw new InputFileDoesntExistException();
    }
}

const char InputFileManager::read()
{
    return this->file->get();
}

void InputFileManager::moveIndicatorBack(int number)
{
    this->file->seekg((-1)*number, std::ios_base::cur );
}

bool InputFileManager::iseof() const
{
    return this->file->eof();

}

