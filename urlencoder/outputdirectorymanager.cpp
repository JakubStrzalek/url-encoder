#include "outputdirectorymanager.h"

#include "exceptions/toomanypatternsexception.h"
#include "filemanager/outputfilemanager.h"

std::string OutputDirectoryManager::format(int number)
{
    std::string number_as_string = std::to_string(number);


    for (int i = number_as_string.length(); i < MAX_NUMBER_OF_DIGIT; ++i)
    {
        number_as_string = "0" + number_as_string;
    }

    return number_as_string;
}

int OutputDirectoryManager::getNextValue() throw (std::exception)
{
    ++previousValue;
    if(previousValue < MAX_EXPRESSION_NUMBER)
    {
        return previousValue;
    } else
    {
        throw TooManyPatternsException(MAX_EXPRESSION_NUMBER);
    }
}

void OutputDirectoryManager::saveFile(std::string value)
{
    std::string numberOfPattern = format(getNextValue());
    std::string fileName("pattern" + numberOfPattern);

    OutputFileManager file(this->directory + fileName);

    file.write(value);

    //wyjście powinno załatwić niszczenie obiektu i zamknięcie pliku
}
