#ifndef TOOMANYPATTERNSEXCEPTION_H
#define TOOMANYPATTERNSEXCEPTION_H

#include <exception>

class TooManyPatternsException : std::exception
{
private:
    const int MAX_VALUE;
    const char *MESSAGE = "Too many pattern exception! Application can't generate patterns more than" + MAX_VALUE;
public:
    TooManyPatternsException(const int MAX_VALUE) : MAX_VALUE(MAX_VALUE) {}

    virtual const char *what() const noexcept override;
};

#endif // TOOMANYPATTERNSEXCEPTION_H
