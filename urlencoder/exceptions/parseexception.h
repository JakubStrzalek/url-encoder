#ifndef PARSEEXCEPTION_H
#define PARSEEXCEPTION_H

#include <exception>

class ParseException : std::exception
{
private:
    const char *MESSAGE = "Error while parsing input file!";
public:
    ParseException();

    virtual const char *what() const noexcept override;
};

#endif // PARSEEXCEPTION_H
