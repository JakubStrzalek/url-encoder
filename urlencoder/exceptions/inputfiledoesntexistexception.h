#ifndef INPUTFILEDOESNTEXISTEXCEPTION_H
#define INPUTFILEDOESNTEXISTEXCEPTION_H

#include <exception>

class InputFileDoesntExistException : std::exception
{
private:
    const char *MESSAGE = "File to parse doesn't exist!";
public:
    InputFileDoesntExistException();

    virtual const char *what() const noexcept override;
};

#endif // INPUTFILEDOESNTEXISTEXCEPTION_H
