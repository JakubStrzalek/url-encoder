#ifndef INVALIDARGUMENTSNUMBEREXCEPTION_H
#define INVALIDARGUMENTSNUMBEREXCEPTION_H

#include <exception>

class InvalidArgumentsNumberException : std::exception
{
private:
    const char *MESSAGE = "Wrong number of arguments";
public:
    InvalidArgumentsNumberException();

    virtual const char *what() const noexcept override;
};

#endif // INVALIDARGUMENTSNUMBEREXCEPTION_H
