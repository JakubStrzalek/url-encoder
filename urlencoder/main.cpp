#include <iostream>
#include "urlencoder.h"

using namespace std;

int main(int size, char *arguments[])
{
    URLencoder *encoder;
    try
    {
        encoder = new URLencoder(size,arguments);
    }
    catch(std::exception e)
    {
        std::cerr<<e.what()<<std::endl;
        return EXIT_FAILURE;
    }

    int solution = encoder->run();
    delete encoder;
    return solution;
}

